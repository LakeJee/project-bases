// function definitions

// char functions
function B32
CharIsAlpha(U8 c)
{
  return CharIsAlphaUpper(c) || CharIsAlphaLower(c);
}

function B32
CharIsAlphaUpper(U8 c)
{
  return c >= 'A' && c <= 'Z';
}

function B32
CharIsAlphaLower(U8 c)
{
  return c >= 'a' && c <= 'z';
}

function B32
CharIsDigit(U8 c)
{
  return (c >= '0' && c <= '9');
}

function B32
CharIsSymbol(U8 c)
{
  return (c == '~' || c == '!'  || c == '$' || c == '%' || c == '^' ||
          c == '&' || c == '*'  || c == '-' || c == '=' || c == '+' ||
          c == '<' || c == '.'  || c == '>' || c == '/' || c == '?' ||
          c == '|' || c == '\\' || c == '{' || c == '}' || c == '(' ||
          c == ')' || c == '\\' || c == '[' || c == ']' || c == '#' ||
          c == ',' || c == ';'  || c == ':' || c == '@');
}

function B32
CharIsSpace(U8 c)
{
  return c == ' ' || c == '\r' || c == '\t' || c == '\f' || c == '\v';
}

function U8
CharToUpper(U8 c)
{
  return (c >= 'a' && c <= 'z') ? ('A' + (c - 'a')) : c;
}

function U8
CharToLower(U8 c)
{
  return (c >= 'A' && c <= 'Z') ? ('a' + (c - 'A')) : c;
}

function U8
CharToForwardSlash(U8 c)
{
  return (c == '\\' ? '/' : c);
}

// helper
function U64
CalculateCStringLength(char *cstr)
{
  U64 length = 0;
  for(;cstr[length]; length += 1);
  return length;
}

// constructors
function String8
Str8(U8 *str, U64 size)
{
  String8 string;
  string.str  = str;
  string.size = size;
  return string;
}

function String8 
Str8FromStr8(String8 str)
{
  String8 string;
  string.str  = str.str;
  string.size = str.size;
  return string;
}

function String8
Str8Concat(String8 str1, String8 str2)
{
  
  String8 result = {0};
  result.size = str1.size + str2.size;
  result.str = str1.str;
  U64 i = 0;
  U64 j = str1.size;
  
  while (i < str2.size)
  {
    result.str[j] = str2.str[i];
    i++;
    j++;
  }
  
  result.str[result.size] = '\0';
  
  return result;
  
  
}

function B32
Str8Compare(String8 str1, String8 str2)
{
  return strcmp((CStr)str1.str, (CStr)str2.str) == 0 ? true : false;
}

function B32
Str8IsHiddenDir(String8 str1)
{
  return str1.str[0] == '.' ? true : false; 
}

function String8
Str8Range(U8 *first, U8 *one_past_last)
{
  String8 string;
  string.str = first;
  string.size = (U64)(one_past_last - first);
  return string;
}

function String16
Str16(U16 *str, U64 size)
{
  String16 result;
  result.str = str;
  result.size = size;
  return result;
}

function String16
Str16C(U16 *ptr)
{
  U16 *p = ptr;
  for (;*p; p += 1);
  String16 result = Str16(ptr, p - ptr);
  return(result);
}

// sub strings
function String8
Substr8(String8 str, U64 min, U64 max)
{
  if(max > str.size)
  {
    max = str.size;
  }
  if(min > str.size)
  {
    min = str.size;
  }
  if(min > max)
  {
    U64 swap = min;
    min = max;
    max = swap;
  }
  str.size = max - min;
  str.str += min;
  return str;
}

function String8
Str8Skip(String8 str, U64 min)
{
  return Substr8(str, min, str.size);
}

function String8
Str8Chop(String8 str, U64 nmax)
{
  return Substr8(str, 0, str.size-nmax);
}

function String8
Prefix8(String8 str, U64 size)
{
  return Substr8(str, 0, size);
}

function String8
Suffix8(String8 str, U64 size)
{
  return Substr8(str, str.size-size, str.size);
}

function B32
Str8Match(String8 a, String8 b, MatchFlags flags)
{
  B32 result = 0;
  if(a.size == b.size || flags & MatchFlag_RightSideSloppy)
  {
    result = 1;
    for(U64 i = 0; i < a.size; i += 1)
    {
      B32 match = (a.str[i] == b.str[i]);
      if(flags & MatchFlag_CaseInsensitive)
      {
        match |= (CharToLower(a.str[i]) == CharToLower(b.str[i]));
      }
      if(flags & MatchFlag_SlashInsensitive)
      {
        match |= (CharToForwardSlash(a.str[i]) == CharToForwardSlash(b.str[i]));
      }
      if(match == 0)
      {
        result = 0;
        break;
      }
    }
  }
  return result;
}

function U64
FindSubstr8(String8 haystack, String8 needle, U64 start_pos, MatchFlags flags)
{
  B32 found = 0;
  U64 found_idx = haystack.size;
  for(U64 i = start_pos; i < haystack.size; i += 1)
  {
    if(i + needle.size <= haystack.size)
    {
      String8 substr = Substr8(haystack, i, i+needle.size);
      if(Str8Match(substr, needle, flags))
      {
        found_idx = i;
        found = 1;
        if(!(flags & MatchFlag_FindLast))
        {
          break;
        }
      }
    }
  }
  return found_idx;
}

function B32
FindSubstr8Bool(String8 haystack, String8 needle, U64 start_pos, MatchFlags flags)
{
  B32 found = 0;
  U64 found_idx = haystack.size;
  for(U64 i = start_pos; i < haystack.size; i += 1)
  {
    if(i + needle.size <= haystack.size)
    {
      String8 substr = Substr8(haystack, i, i+needle.size);
      
      if(Str8Match(substr, needle, flags))
      {
        found_idx = i;
        found = 1;
        if(!(flags & MatchFlag_FindLast))
        {
          break;
        }
      }
    }
  }
  
  return found == 1 ? true : false;
}

// string lists
function void
Str8ListPushNode(String8List *list, String8Node *n)
{
  QueuePush(list->first, list->last, n);
  list->node_count += 1;
  list->total_size += n->string.size;
}

function void
Str8ListPush(M_Arena *arena, String8List *list, String8 str)
{
  String8Node *n = PushArrayZero(arena, String8Node, 1);
  n->string = str;
  Str8ListPushNode(list, n);
}

function B32
Str8IsEmpty(String8 str)
{
  return str.str == NULL || str.size == 0;
}

function void
Str8StackNodePush(M_Arena *arena, String8StackNode **stack, String8 str)
{
  if (!(*stack))
  {
    (*stack) = PushArrayZero(arena, String8StackNode, 1);
  }
  if (Str8IsEmpty((*stack)->string))
  {
    (*stack)->string = str;
    (*stack)->next   = 0;
  } else {
    String8StackNode *n = PushArrayZero(arena, String8StackNode, 1);
    n->string = (*stack)->string;
    (*stack)->next = n;
    (*stack)->string = str;
  }
}

function String8
Str8StackNodePop(String8StackNode **stack)
{
  String8 result = {0};
  if ((*stack))
  {
    result = (*stack)->string;
    (*stack) = (*stack)->next;
  }
  return result;
}

function void
Str8ListPop(String8List *list)
{
  if (list->first == list->last || list->node_count == 1)
  {
    list->first = list->last;
    list->node_count = 1;
  } else {
    list->first = list->first->next;
    list->node_count -= 1;
  }
}

function String16
Str16From8(String8 s)
{
  String16 result = {0};
  result.str = (U16 *)s.str;
  result.size = s.size;
  return result;
}

function void
Str16ListPushNode(String16List *list, String16Node *n)
{
  QueuePush(list->first, list->last, n);
  list->node_count += 1;
  list->total_size += n->string.size;
}

function void
Str16ListPush(M_Arena *arena, String16List *list, String16 str)
{
  String16Node *n = PushArrayZero(arena, String16Node, 1);
  n->string = str;
  Str16ListPushNode(list, n);
}

function void
Str8ListConcat(String8List *list, String8List *to_push)
{
  if(to_push->first)
  {
    list->node_count += to_push->node_count;
    list->total_size += to_push->total_size;
    if(list->last == 0)
    {
      *list = *to_push;
    }
    else
    {
      list->last->next = to_push->first;
      list->last = to_push->last;
    }
  }
  MemoryZero(to_push, sizeof(*to_push));
}

function String8List
StrSplit8(M_Arena *arena, String8 string, int split_count, String8 *splits)
{
  String8List list = {0};
  
  U64 split_start = 0;
  for(U64 i = 0; i < string.size; i += 1)
  {
    B32 was_split = 0;
    for(int split_idx = 0; split_idx < split_count; split_idx += 1)
    {
      B32 match = 0;
      if(i + splits[split_idx].size <= string.size)
      {
        match = 1;
        for(U64 split_i = 0; split_i < splits[split_idx].size && i + split_i < string.size; split_i += 1)
        {
          if(splits[split_idx].str[split_i] != string.str[i + split_i])
          {
            match = 0;
            break;
          }
        }
      }
      if(match)
      {
        String8 split_string = Str8(string.str + split_start, i - split_start);
        Str8ListPush(arena, &list, split_string);
        split_start = i + splits[split_idx].size;
        i += splits[split_idx].size - 1;
        was_split = 1;
        break;
      }
    }
    
    if(was_split == 0 && i == string.size - 1)
    {
      String8 split_string = Str8(string.str + split_start, i+1 - split_start);
      Str8ListPush(arena, &list, split_string);
      break;
    }
  }
  
  return list;
}

function String8
Str8ListJoin(M_Arena *arena, String8List list, StringJoin *optional_params)
{
  // rjf: setup join parameters
  StringJoin join = {0};
  if(optional_params != 0)
  {
    MemoryCopy(&join, optional_params, sizeof(join));
  }
  
  // rjf: calculate size & allocate
  U64 sep_count = 0;
  if(list.node_count > 1)
  {
    sep_count = list.node_count - 1;
  }
  String8 result = {0};
  result.size = (list.total_size + join.pre.size +
                 sep_count*join.sep.size + join.post.size);
  result.str = PushArray(arena, U8, result.size);
  
  // rjf: fill
  U8 *ptr = result.str;
  MemoryCopy(ptr, join.pre.str, join.pre.size);
  ptr += join.pre.size;
  for(String8Node *node = list.first; node; node = node->next)
  {
    MemoryCopy(ptr, node->string.str, node->string.size);
    ptr += node->string.size;
    if (node != list.last){
      MemoryCopy(ptr, join.sep.str, join.sep.size);
      ptr += join.sep.size;
    }
  }
  MemoryCopy(ptr, join.pre.str, join.pre.size);
  ptr += join.pre.size;
  
  return result;
}