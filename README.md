# Project Bases

## Purpose
This repository will act as a collection of scripts to automate the process of setting up a new code base for the stuff I want to work on!

Feel free to clone and edit things as you see fit!

| Platform    | Programming Language  | Version  |
| :---------: | :-------------------: | :------: |
| Windows     | C/C++                 |  1.0.2   |
| Linux       | TBA                   |  TBA     |
| MacOS       | TBA                   |  TBA     | 
