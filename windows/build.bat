@ECHO off

REM Primary build script for building project_name

REM -------------------------------------
REM Variables
REM -------------------------------------

SET COMPILER_OPTS=/Wall /MT /FC /Zi /nologo /I
SET INCLUDE_DIR=..\code\include
SET SOURCE_FILES=..\code\src\*.c
SET LIB_FILES=..\code\lib\*.lib

REM -------------------------------------
REM Main
REM -------------------------------------

CALL :CompilerCheck
CALL :Build
CALL :project_nameCompile
CALL :ReturnToCode
GOTO :EOF

REM -------------------------------------
REM Functions
REM -------------------------------------

REM Check for compiler.
:CompilerCheck
ECHO Creating environment to build project_name in.
WHERE /q cl
IF ERRORLEVEL 1 (
  	ECHO Program: cl.exe [Windows C/C++ compiler] is missing. Ensure it is installed and placed in your PATH.
		ECHO Hint: Install Visual Studio and run `vcvarsall.bat x64` in your command line.
		ECHO Hint: If you already have it Visual Studio installed, run `shell.bat` in your command line.
		CALL vcvarsall.bat x64    
) ELSE (
  	ECHO Program: cl.exe exists. Let's go!
)
GOTO :EOF


REM Checks for build directory.
:Build
ECHO Checking for build directory.
IF EXIST ..\build (
	ECHO Directory::..\build already exists. Skipping.
	ECHO Cleaning Directory::..\build.
	PUSHD ..\build >nul
	DEL /F /Q *.*
	POPD >nul
	ECHO Done.
) ELSE (
	ECHO Creating Directory::..\build.
	MKDIR build
)
GOTO :EOF


REM Function to run compile process and check compile status
:project_nameCompile
PUSHD ..\build >nul
ECHO.
ECHO Starting build process.
ECHO.

cl.exe %COMPILER_OPTS% %INCLUDE_DIR% %SOURCE_FILES% %LIB_FILES% /Fe:project_name.exe

ECHO.
IF %ERRORLEVEL% EQU 0 (
	ECHO Success::project_name.exe::Generated.
	ECHO.
) 
IF %ERRORLEVEL% NEQ 0 (
	ECHO Failure::project_name.exe::Not Generated.
	ECHO.
)
GOTO :EOF


REM Function return to code directory
:ReturnToCode
ECHO.
ECHO Returning to Directory::..\code.
POPD >nul
GOTO :EOF