function 
String8 get_current_working_dir (void)
{
  String8 getCwResult = {0};
  
  if ((getCwResult = Str8C(_getcwd (NULL, 0))).str == NULL) 
  {
    LogError("_getcwd returned NULL");
    String8 emptyString = {0};
    return emptyString;
  } else {
    return getCwResult;
  }
}

// Assume user is looking for directory if `isFile` is false
function 
String8 find (M_Arena *arena, String8 name, B32 isFile, B32 recursive)
{
  String8 workingDir = get_current_working_dir();
  
  Win32FindData win32FileVars = {0};
  win32FileVars.hFind         = INVALID_HANDLE_VALUE;
  win32FileVars.dwError       = 0;
  
  String8 result         = {0};
  String8 dirSepAll      = Str8C(FILE_SEPARATOR_ALL);
  String8 dirSep         = Str8C(FILE_SEPARATOR);
  String8 currentDirDot  = Str8C(CURRENT_DIR_DOT);
  String8 previousDirDots = Str8C(PREVIOUS_DIR_DOTS);
  
  if (name.str == NULL || name.size == 0)
  {
    LogError("Invalid file or directory name.");
    String8 empty = {0};
    return (empty);
  } else if (workingDir.size > (MAX_PATH - 3)) {
    LogError("Current working directory exceeds the max path length.");
    String8 empty = {0};
    return (empty);
  } else {
    String8 initialHFindMe = {0};
    {
      String8List list = {0};
      Str8ListPush(arena, &list, workingDir);
      Str8ListPush(arena, &list, dirSepAll);
      initialHFindMe = Str8ListJoin(arena, list, 0);
    }
    
    win32FileVars.hFind = FindFirstFile((CStr)initialHFindMe.str, &win32FileVars.ffd);
    if (win32FileVars.hFind == INVALID_HANDLE_VALUE)
    {
      LogError("Invalid handle value returned from initial FindFirstFile() loop.");
      String8 empty = {0};
      return empty;
    }
    
    String8StackNode *directoryListS = PushArrayZero(arena, String8StackNode, 1);
    Str8StackNodePush(arena, &directoryListS, workingDir);
    
    while(directoryListS)
    {
      String8 currentDir = Str8StackNodePop(&directoryListS);
      
      String8 hFindMe = {0};
      {
        String8List list = {0};
        Str8ListPush(arena, &list, currentDir);
        Str8ListPush(arena, &list, dirSepAll);
        hFindMe = Str8ListJoin(arena, list, 0);
      }
      
      win32FileVars.hFind = FindFirstFile((CStr)hFindMe.str, &win32FileVars.ffd);
      if (win32FileVars.hFind == INVALID_HANDLE_VALUE)
      {
        LogError("Invalid handle value returned from FindFirstFile().");
        String8 empty = {0};
        return empty;
      }
      
      do
      {
        String8 cFileName   = Str8C(win32FileVars.ffd.cFileName);
        
        if (!Str8Compare(cFileName, currentDirDot)
            && !Str8Compare(cFileName, previousDirDots))
        {
          {
            String8List list = {0};
            Str8ListPush(arena, &list, currentDir);
            Str8ListPush(arena, &list, dirSep);
            Str8ListPush(arena, &list, cFileName);
            win32FileVars.szDir = Str8ListJoin(arena, list, 0);
          }
          
          if (win32FileVars.ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
          {
            // lake: push dir name on the stack if user wants to "recursively" search
            if (recursive)
            {
              Str8StackNodePush(arena, &directoryListS, win32FileVars.szDir);
            }
            
            
            // lake: if user is looking for a directory
            if (!isFile && Str8Compare(name, cFileName))
            {
              result = cFileName;
            }
          } else {
            if (Str8Compare(name, cFileName))
            {
              result = cFileName;
            }
          }
        }
      } while (FindNextFile(win32FileVars.hFind, &win32FileVars.ffd));
    }
    
    FindClose(win32FileVars.hFind);
  }
  return result;
}

function 
String8 find_directory(M_Arena *arena, String8 dirName)
{
  return find(arena, dirName, false, false);
}

function 
String8 find_file (M_Arena *arena, String8 fileName)
{
  return find(arena, fileName, true, false);
}

function 
String8 find_directory_recursive(M_Arena *arena, String8 dirName)
{
  return find(arena, dirName, false, true);
}

function 
String8 find_file_recursive (M_Arena *arena, String8 fileName)
{
  return find(arena, fileName, true, true);
}