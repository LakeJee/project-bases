/* date = December 16th 2022 10:19 am */

#ifndef BASE_INC_H
#define BASE_INC_H


// disable annoying as fuck warings for including windows.h
#pragma warning (disable : 4668)
#pragma warning (disable : 4710)
#pragma warning (disable : 5045)
#pragma warning (disable : 4255)
#pragma warning (disable : 4820)

#pragma comment (lib, "User32.lib")


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <windows.h>
#include <windowsx.h>
#include <tlhelp32.h>
#include <Shlobj.h>
#include <direct.h>
#include <stdbool.h>
#include <tchar.h>
#include <strsafe.h>
#include <fileapi.h>
#include <math.h>
#include <time.h>
#include <assert.h>

#include "base_types.h"
#include "base_memory.h"
#include "base_strings.h"


#endif //BASE_INC_H
