/* date = December 16th 2022 10:41 am */

#ifndef OS_H
#define OS_H

#define M_IMPL_Reserve  OS_Reserve
#define M_IMPL_Release  OS_Release
#define M_IMPL_Commit   OS_Commit
#define M_IMPL_Decommit OS_Decommit


function U64   OS_PageSize(void);
function void *OS_Reserve(U64 size);
function void  OS_Release(void *ptr);
function void  OS_Commit(void *ptr, U64 size);
function void  OS_Decommit(void *ptr, U64 size);


#endif //OS_H
