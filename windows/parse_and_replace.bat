@ECHO off

SETLOCAL EnableExtensions EnableDelayedExpansion
SET INTEXTFILE=%1
SET OUTTEXTFILE=temp_out.txt
SET SEARCHTEXT=project_name
SET REPLACETEXT=%2

FOR /f "delims=" %%A in ('type "%INTEXTFILE%"') DO (
    SET "string=%%A"
    SET "modified=!string:%SEARCHTEXT%=%REPLACETEXT%!"
    ECHO !modified!>>"%OUTTEXTFILE%"
)

DEL "%INTEXTFILE%"
RENAME "%OUTTEXTFILE%" "%INTEXTFILE%"
ENDLOCAL