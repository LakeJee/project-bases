@ECHO off

REM This script will ideally setup a working build environment to create C apps on Windows
REM Arguments: 

REM -------------------------------------
REM Variables
REM -------------------------------------


REM -------------------------------------
REM Define your "Project Bases" base here
REM -------------------------------------
SET PROJECT_BASES_BASE=
:::::::::::::::::::::::::::::::::::::::::


SET CURRENT_DIR="%cd%"
SET NEW_PROJECT_BASE=%CURRENT_DIR%
SET NEW_PROJECT_BASE=###%NEW_PROJECT_BASE%###
SET NEW_PROJECT_BASE=%NEW_PROJECT_BASE:"###=%
SET NEW_PROJECT_BASE=%NEW_PROJECT_BASE:###"=%
SET NEW_PROJECT_BASE=%NEW_PROJECT_BASE:###=%
SET CODE_DIR_NAME=code
SET BUILD_DIR_NAME=build
SET DEBUG_DIR_NAME=debug
SET INCLUDE_DIR_NAME=incldue
SET CODE_DIR=%CURRENT_DIR%\%CODE_DIR_NAME% 
SET BUILD_DIR=%CURRENT_DIR%\%BUILD_DIR_NAME%
SET DEBUG_DIR=%CURRENT_DIR%\%DEBUG_DIR_NAME%
SET INCLUDE_DIR=%CURRENT_DIR%\%INCLUDE_DIR_NAME%
SET PROJECT_NAME=%1
SET REPLACE_PROJECT_NAME="project_name"
SET BUILD_SCRIPT=build.bat
SET BUILD_CLANG_SCRIPT=build_clang.bat
SET DEBUG_SECRIPT=debug.bat
SET FORMAT_SCRIPT=format.bat
SET MAIN_CODE_FILE=main.c
SET README_FILE_NAME=README.md

REM -------------------------------------
REM Main
REM -------------------------------------

CALL :ProjectBasesCheck
IF %EXIT_CODE% NEQ 999 (
	CALL :CreateDirectories
	CALL :CreateSubdirectoriesInCode
	CALL :MoveBuildDebugFormatAndMainToCodeDirectory
	CALL :ParseProjectNameToBuildScript
	CALL :ParseProjectNameToBuildClangScript
	CALL :ParseProjectNameToReadMe
	CALL :CleanUp
	GOTO :EOF
) ELSE (
	GOTO :EOF
)

REM -------------------------------------
REM Functions
REM -------------------------------------

REM Function to check for project bases
:ProjectBasesCheck
IF [%PROJECT_BASES_BASE%] == [] (
	ECHO.
	ECHO Variable PROJECT_BASES_BASE not defined!
	ECHO Hint: Whatever directory you cloned this repo into, set it to that!
	ECHO Hint: Once that is done, add this directory to your path!
	SET EXIT_CODE=999
) ELSE (
	ECHO.
	ECHO Hi there! Glad you made it!
	SET EXIT_CODE=1
)
GOTO :EOF

REM Function that will create directories at the project base 
:CreateDirectories
ECHO.
ECHO Creating directories at project base: %NEW_PROJECT_BASE%
IF EXIST build (
	ECHO Directory::build::Already exists. Skipping.
) ELSE (
	ECHO Creating Directory::build.
	MKDIR build
)
IF EXIST code (
	ECHO Directory::code::Already exists. Skipping.
) ELSE (
	ECHO Creating Directory::code.
	MKDIR code
)
IF EXIST debug (
	ECHO Directory::debug::Already exists. Skipping.
) ELSE (
	ECHO Creating Directory::debug.
	MKDIR debug
)
GOTO :EOF


REM Function that creats subdirectories
:CreateSubdirectoriesInCode
PUSHD code >nul
IF EXIST src (
	ECHO Directory::code\src::Already exists. Skipping.
) ELSE (
	ECHO Creating Directory::code\src.
	MKDIR src
)
IF EXIST lib (
	ECHO Directory::code\lib::Already exists. Skipping.
) ELSE (
	ECHO Creating Directory::code\lib.
	MKDIR lib
)
IF EXIST include (
	ECHO Directory::code\include::Already exists. Skipping.
) ELSE (
	ECHO Creating Directory::inlcude directory will be added during file copy.
)
POPD code >nul
GOTO :EOF


REM Function that adds build.bat, debug.bat, and main.c
:MoveBuildDebugFormatAndMainToCodeDirectory
ECHO.
ECHO Copying files to project.
IF EXIST code (
	PUSHD %CODE_DIR% >nul
	ECHO D | XCOPY %PROJECT_BASES_BASE%\* /S /V /E /Y /F >nul
	MOVE %MAIN_CODE_FILE% src\ >nul
	MOVE %README_FILE_NAME% ..\ >nul
	POPD >nul
	ECHO Success::Files copied to code directory
	POPD >nul
) ELSE (
	ECHO ERROR::Directory \code\::Not found!
)
GOTO :EOF


REM Function to replace 'project_name' with user defined project name (passed as the first argument) in build.bat
:ParseProjectNameToBuildScript
ECHO.
PUSHD %CODE_DIR% >nul
IF %PROJECT_NAME% == "" (
	ECHO Project name not set.
	EHCO NOTE: You will need to replace 'project_name' in your build.bat later.
) ELSE (
	ECHO Beginning parse and replace of %BUILD_SCRIPT%.

	CALL parse_and_replace.bat %BUILD_SCRIPT% %PROJECT_NAME%

	ECHO Check build.bat to be sure this step completed correctly.
)
POPD >nul
GOTO :EOF


REM Function to replace 'project_name' with user defined project name (passed as the first argument) in build_clang.bat
:ParseProjectNameToBuildClangScript
ECHO.
PUSHD %CODE_DIR% >nul
IF %PROJECT_NAME% == "" (
	ECHO Project name not set.
	EHCO NOTE: You will need to replace 'project_name' in your build.bat later.
) ELSE (
	ECHO Beginning parse and replace of %BUILD_CLANG_SCRIPT%.

	CALL parse_and_replace.bat %BUILD_CLANG_SCRIPT% %PROJECT_NAME%

	ECHO Check build_clang.bat to be sure this step completed correctly.
)
POPD >nul
GOTO :EOF


REM Function to replace 'project_name' with user defined project name (passed as the first argument) in build.bat
:ParseProjectNameToReadMe
ECHO.
PUSHD %NEW_PROJECT_BASE% >nul
IF %PROJECT_NAME% == "" (
	ECHO Project name not set.
	EHCO NOTE: You will need to replace 'project_name' in your README.md later.
) ELSE (
	ECHO Beginning parse and replace of %README_FILE_NAME%.
	
	CALL parse_and_replace.bat %README_FILE_NAME%  %PROJECT_NAME%

	ECHO Check README.md to be sure this step completed correctly.
)
POPD >nul
GOTO :EOF


REM Function that cleans up copied files
:CleanUp
ECHO.
PUSHD %CODE_DIR% >nul
DEL /F init_empty_project.bat
DEL /F parse_and_replace.bat
POPD >nul
ECHO Project setup complete!
GOTO :EOF