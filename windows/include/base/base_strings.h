/* date = December 16th 2022 10:01 am */

#ifndef BASE_STRINGS_H
#define BASE_STRINGS_H

// some string stuff from rjf
typedef struct String8 String8;
struct String8
{
  U8 *str;
  U64 size;
};

typedef struct String16 String16;
struct String16
{
  U16 *str;
  U64 size;
};

typedef struct String32 String32;
struct String32
{
  U32 *str;
  U64 size;
};

typedef struct String8Node String8Node;
struct String8Node
{
  String8Node *next;
  String8 string;
};

typedef struct String8List String8List;
struct String8List
{
  String8Node *first;
  String8Node *last;
  U64 node_count;
  U64 total_size;
};

typedef struct String8StackNode String8StackNode;
struct String8StackNode
{
  String8StackNode *next;
  String8 string;
};

typedef struct String8Stack String8Stack;
struct String8Stack
{
  String8StackNode *top;
  U64 total_size;
};

typedef struct String16Node String16Node;
struct String16Node
{
  String16Node *next;
  String16 string;
};

typedef struct String16List String16List;
struct String16List
{
  String16Node *first;
  String16Node *last;
  U64 node_count;
  U64 total_size;
};

typedef struct StringJoin StringJoin;
struct StringJoin
{
  String8 pre;
  String8 sep;
  String8 post;
};

typedef U32 MatchFlags;
enum
{
  MatchFlag_CaseInsensitive  = (1<<0),
  MatchFlag_RightSideSloppy  = (1<<1),
  MatchFlag_SlashInsensitive = (1<<2),
  MatchFlag_FindLast         = (1<<3),
};

// some char functions from rjf
function B32 CharIsAlpha(U8 c);
function B32 CharIsAlphaUpper(U8 c);
function B32 CharIsAlphaLower(U8 c);
function B32 CharIsDigit(U8 c);
function B32 CharIsSymbol(U8 c);
function B32 CharIsSpace(U8 c);
function U8  CharToUpper(U8 c);
function U8  CharToLower(U8 c);
function U8  CharToForwardSlash(U8 c);


// helper function from rjf
function U64 CalculateCStringLength(char *cstr);


// some helpers from rjf
function String8 Str8(U8 *str, U64 size);
function String8 Str8FromStr8(String8 str);
function String8 Str8Concat(String8 str1, String8 str2);
function String8 Str8ConcatAll(String8 str1, ...);
function B32 Str8Compare(String8 str1, String8 str2);
function B32 Str8IsHiddenDir(String8 str1);
#define Str8C(cstring) Str8((U8 *)(cstring), CalculateCStringLength(cstring))
#define Str8Lit(s) Str8((U8 *)(s), sizeof(s)-1)
#define Str8LitComp(s) {(U8 *)(s), sizeof(s)-1}
function String8 Str8Range(U8 *first, U8 *one_past_last);
function String16 Str16(U16 *str, U64 size);
function String16 Str16C(U16 *ptr);
#define Str16CStr(cstring) Str16((U16 *)cstring, CalculateCStringLength(cstring));
function String16 Str16From8(String8 s);
function void Str16Concat(String16 src, String16 dest);
#define Str8Struct(ptr) Str8((U8 *)(ptr), sizeof(*(ptr)));
function String8 Substr8(String8 str, U64 min, U64 max);
function String8 Str8Skip(String8 str, U64 min);
function String8 Str8Chop(String8 str, U64 nmax);
function String8 Prefix8(String8 str, U64 size);
function String8 Suffix8(String8 str, U64 size);
function B32 Str8Match(String8 a, String8 b, MatchFlags flags);
function U64 FindSubstr8(String8 haystack, String8 needle, U64 start_pos, MatchFlags flags);
function B32 FindSubStr8Bool(String8 haystack, String8 needle, U64 start_pos, MatchFlags flags);

// list helpers from rjf
function void Str8ListPushNode(String8List *list, String8Node *n);
function void Str8ListPush(M_Arena *arena, String8List *list, String8 str);
function void Str8ListPop(String8List *list);
function void Str16ListPushNode(String16List *list, String16Node *n);
function void Str16ListPush(M_Arena *arena, String16List *list, String16 str);
function void Str8ListConcat(String8List *list, String8List *to_push);
function void Str8StackNodePush(M_Arena *arena, String8StackNode **stack, String8 str);
function String8 Str8StackNodePop(String8StackNode **stack);
function String8StackNode Str8SNodeFromStr8(String8 str);
function String8List StrSplit8(M_Arena *arena, String8 string, int split_count, String8 *splits);
function String8 Str8ListJoin(M_Arena *arena, String8List list, StringJoin *optional_params);

#endif //BASE_STRINGS_H
