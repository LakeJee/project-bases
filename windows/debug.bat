@ECHO off

REM This script will attempt to look for remedybg.exe in your path first.
REM Then it will call and attach remedy to the simp.exe process

REM -------------------------------------
REM Variables
REM -------------------------------------


REM -------------------------------------
REM Main
REM -------------------------------------
CALL :remedyCheck
CALL :callBuild
CALL :simpCheck

IF [%1] NEQ [] (
	CALL :debugAtFunction
) ELSE (
	CALL :debugStart
)

REM -------------------------------------
REM Functions
REM -------------------------------------

REM Check for remedybg.exe
:remedyCheck
where /q remedybg.exe
	IF ERRORLEVEL 1 (
    	ECHO The application is missing. Ensure it is installed and placed in your PATH.    
	) ELSE (
    	ECHO remedybg.exe exists. Let's go!
	)
GOTO :EOF


REM Call the build script to generate a build
:callBuild
IF EXIST build.bat (
	ECHO Calling build script.
	CALL build.bat
) ELSE (
	ECHO No build script found.
	ECHO Aborting.
)
GOTO :EOF


REM Check for simp directory
:simpCheck
IF EXIST ..\build\simp.exe (
	ECHO Executable: simp.exe exists.
	PUSHD ..\build
) ELSE (
	ECHO You can generate simp.exe  with a successful build.
)
GOTO :EOF


REM Run remedy at function name
:debugAtFunction
ECHO Starting remedy. Debugging at %1
remedybg.exe add-breakpoint-at-function %1
GOTO :EOF


REM Run remedy on simp
:debugStart
ECHO Starting remedy.
remedybg.exe -g simp.exe
GOTO :EOF


REM Function that returns to code dir
ECHO.
	ECHO Returning to Directory: ..\code.
  CD ..\code
GOTO :EOF