/* date = February 11th 2023 3:58 pm */

#ifndef BASE_FILES_H
#define BASE_FILES_H

// lake: file constants
#define FILE_SEPARATOR        "\\"
#define FILE_SEPARATOR_ALL    "\\*"
#define CURRENT_DIR_DOT       "."
#define PREVIOUS_DIR_DOTS     ".."


// lake: file structs
typedef struct Win32FindData Win32FindData;
struct Win32FindData 
{
  B32             found;
  WIN32_FIND_DATA ffd;
  String8         szDir;
  size_t          lengthOfDir;
  HANDLE          hFind;
  DWORD           dwError;
};


// lake: file functions
function String8 get_current_working_dir  (void);
function String8 find                     (M_Arena *arena, String8 name, B32 isFileOrDir, B32 recursive);
function String8 find_directory           (M_Arena *arena, String8 dirName);
function String8 find_file                (M_Arena *arena, String8 fileName);
function String8 find_directory_recursive (M_Arena *arena, String8 dirName);
function String8 find_file_recursive      (M_Arena *arena, String8 fileName);

#endif //BASE_FILES_H
