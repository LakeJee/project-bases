@ECHO off

REM Primary build script using clang compiler for project_name

REM -------------------------------------
REM Variables
REM -------------------------------------

SET CLANG_COMPILER_OPTS=-O3 -g -m64
SET CLANG_LINK_FLAGS=-fuse-ld=lld
SET INCLUDE_DIR=-I ..\code\include
SET SOURCE_FILES=..\code\src\*.c
SET LIB_FILES=..\code\lib\*.lib
SET INC_EMPTY=1
SET LIB_EMPTY=1

REM -------------------------------------
REM Main
REM -------------------------------------

CALL :PlatformCheck
CALL :CompilerCheck
CALL :Build
CALL :ClangPracCompile
CALL :ReturnToCode
GOTO :EOF

REM -------------------------------------
REM Functions
REM -------------------------------------

REM Function to check platform
:PlatformCheck
IF "%Platform%" NEQ "x64" (
	ECHO ERROR::Platform is not x64
	ECHO HINT::Install Visual Studio and run `vcvarsall.bat x64` in your command line.
  ECHO HINT::If you already have it Visual Studio installed, run `shell.bat` in your command line.
	EXIT /b 1
)
GOTO :EOF


REM Function to check for compiler
:CompilerCheck
WHERE /q clang 
IF %ERRORLEVEL% EQU 1 (
	ECHO ERROR::clang.exe not found!
	ECHO HINT::Go to https://clang.llvm.org/ to install and add to PATH.
) ELSE (
	ECHO clang.exe exists. Let's go!
)
GOTO :EOF


REM Checks for build directory.
:Build
ECHO Checking for build directory.
IF EXIST ..\build (
        ECHO Directory::..\build already exists. Skipping.
        ECHO Cleaning Directory::..\build.
        PUSHD ..\build >nul
        DEL /F /Q *.*
        POPD >nul
        ECHO Done.
) ELSE (
        ECHO Creating Directory::..\build.
        MKDIR build
)
GOTO :EOF


REM Function to run compile process and check compile status
:ClangPracCompile
PUSHD ..\build >nul
ECHO.
ECHO Starting build process.
ECHO.

SET COMPILE_STRING=clang.exe %CLANG_COMPILER_OPTS% %CLANG_LINK_FLAGS% %INCLUDE_DIR% %SOURCE_FILES% -o project_name.exe

for /F %%i in ('dir /b /a "..\code\lib\*"') do (
		%LIB_EMTPY%=999
)

%COMPILE_STRING%=

IF %LIB_EMPTY% NEQ 1 (
	%COMPILE_STRING%=%COMPILE_STRING% %LIB_FILES%
)

CALL %COMPILE_STRING%

ECHO.
IF %ERRORLEVEL% EQU 0 (
        ECHO Success::ClangPrac.exe::Generated.
        ECHO.
)
IF %ERRORLEVEL% NEQ 0 (
        ECHO Failure::ClangPrac.exe::Not Generated.
        ECHO.
)
GOTO :EOF


REM Function return to code directory
:ReturnToCode
ECHO.
ECHO Returning to Directory::..\code.
POPD >nul
GOTO :EOF