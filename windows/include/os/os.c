// OS Gloabsl (win32 only atm)
global HINSTANCE w32_hinstance = 0;
global M_Arena *w32_perm_arena = 0;
global B32 w32_got_system_info = 0;
global SYSTEM_INFO w32_system_info = {0};
global SRWLOCK w32_mutex = SRWLOCK_INIT;
global HMODULE w32_advapi_dll = 0;
global BOOL (*RtlGenRandom)(VOID *RandomBuffer, ULONG RandomBufferLength);
global String8 w32_initial_path = {0};
global LARGE_INTEGER w32_counts_per_second = {0};


// functions
function U64
OS_PageSize(void)
{
  if(w32_got_system_info == 0)
  {
    w32_got_system_info = 1;
    GetSystemInfo(&w32_system_info);
  }
  return w32_system_info.dwPageSize;
}

function void *
OS_Reserve(U64 size)
{
  U64 gb_snapped_size = size;
  gb_snapped_size += Gigabytes(1) - 1;
  gb_snapped_size -= gb_snapped_size%Gigabytes(1);
  void *ptr = VirtualAlloc(0, gb_snapped_size, MEM_RESERVE, PAGE_NOACCESS);
  return ptr;
}

function void
OS_Release(void *ptr)
{
  VirtualFree(ptr, 0, MEM_RELEASE);
}

function void
OS_Commit(void *ptr, U64 size)
{
  U64 page_snapped_size = size;
  page_snapped_size += OS_PageSize() - 1;
  page_snapped_size -= page_snapped_size%OS_PageSize();
  VirtualAlloc(ptr, page_snapped_size, MEM_COMMIT, PAGE_READWRITE);
}

function void
OS_Decommit(void *ptr, U64 size)
{
  VirtualFree(ptr, size, MEM_DECOMMIT);
}